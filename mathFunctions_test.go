package integrationTest

import "testing"

func TestAdd(t *testing.T) {
	answer := add(5, 5)

	if answer != 10 {
		t.Errorf("Add Error, want:10, got:%b", answer)
	}

	answer = add(1.7976931348623157e+308, 5)

	if answer != 0 {
		t.Errorf("Add Error, want:0, got:%b", answer)
	}

	answer = add(-1.7976931348623157e+308, -1.7976931348623157e+308)

	if answer != 0 {
		t.Errorf("Add Error, want:0, got:%b", answer)
	}
}

func TestSubtract(t *testing.T) {
	answer := subtract(4, 3)

	if answer != 1 {
		t.Errorf("Subtract Error, want:1, got:%b", answer)
	}

	answer = subtract(-1.7976931348623157e+308, 3)

	if answer != 0 {
		t.Errorf("Subtract Error, want:0, got:%b", answer)
	}

	answer = subtract(-6, 1.7976931348623157e+308)

	if answer != 0 {
		t.Errorf("Subtract Error, want:0, got:%b", answer)
	}
}
