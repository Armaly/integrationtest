package integrationTest

import "fmt"

func add(num1 float64, num2 float64) float64 {

	if num1 >= 1.7976931348623157e+308 || num2 >= 1.7976931348623157e+308 || num1 <= -1.7976931348623157e+308 || num2 <= -1.7976931348623157e+308 {
		return 0
	}
	return num1 + num2
}

func subtract(num1 float64, num2 float64) float64 {

	if num1 >= 1.7976931348623157e+308 || num2 >= 1.7976931348623157e+308 || num1 <= -1.7976931348623157e+308 || num2 <= -1.7976931348623157e+308 {
		return 0
	}
	return num1 - num2
}

func multiply(num1 float64, num2 float64) float64 {

	if num1 >= 1.7976931348623157e+308 || num2 >= 1.7976931348623157e+308 || num1 <= -1.7976931348623157e+308 || num2 <= -1.7976931348623157e+308 {
		return 0
	} else {
		fmt.Println("totally untested man")
	}
	return num1 * num2
}
